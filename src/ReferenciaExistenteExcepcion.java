
public class ReferenciaExistenteExcepcion extends Exception {
    public ReferenciaExistenteExcepcion(String message){
        super(message);
    }
}
