public class Unifamiliar extends Viviendas {
    float metros_jardin;

    Unifamiliar(String referencia, int metros, int habitaciones, int banos, int zona, int plantas, float metros_jardin){
        super(referencia, metros, habitaciones, banos, zona, plantas);
        this.metros_jardin = metros_jardin;
    }
}
