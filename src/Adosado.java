public class Adosado extends Unifamiliar implements Alquiler {
    int adosados_vecindario;

    Adosado(String referencia, int metros, int habitaciones, int banos, int zona, int plantas, int adosados_vecindario, float metros_jardin){
        super(referencia, metros, habitaciones, banos, zona, plantas, metros_jardin);
        this.adosados_vecindario = adosados_vecindario;
    }

    @Override
    public float calcularAlquiler(Viviendas vivienda) {
        switch (zona){
            case 1:
                return (7.5f * metros) + 85f;
            case 2:
                return (6.5f * metros) + 85f;
            case 3:
                return (5f * metros) + 85f;
            case 4:
                return (4.5f * metros) + 85f;
        }
        return 0f;
    }
}
