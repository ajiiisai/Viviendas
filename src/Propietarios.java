public class Propietarios {
    String nombre, DNI, direccion, telefono;
    int edad;
    /*String*/ Viviendas [] viviendas;

    Propietarios(String nombre, String DNI, int edad, String direccion, String telefono, /*String*/Viviendas[] viviendas){
        this.nombre = nombre;
        this.DNI = DNI;
        this.edad = edad;
        this.direccion = direccion;
        this.telefono = telefono;
        this.viviendas = viviendas;
    }

    public static boolean compruebaDNI(String dni) {
        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        String numeros = "0123456789";
        String NIE = "XYZ";
        int pos;
        String numDni="";
        //Si no tiene longitud 9 ya no sigo
        if (dni.length() == 9) {
            String primera = dni.substring(0, 1);
            //Si la primera letra es de dni extranjero
            if (NIE.indexOf(primera.toUpperCase()) != -1) {
                String num;
                if (primera.toUpperCase().equals("X")) {
                    num = "0";
                } else if (primera.toUpperCase().equals("Y")) {
                    num = "1";
                } else {
                    num = "2";
                }
                numDni = num + dni.substring(1, 8);
            } else {
                numDni = dni.substring(0, dni.length() - 1);
            }

            try {
                int dniNumero = Integer.parseInt(numDni);
                pos = dniNumero % 23;
                if (!dni.substring(dni.length() - 1, dni.length()).equalsIgnoreCase(letras.substring(pos, pos + 1))) {
                    return false;
                }
            } catch (NumberFormatException ex) {
                System.out.println("Hay letras en la parte numérica");
                return false;
            }
            return true;
        }else{
            return false;
        }
    }
}
