public class Viviendas implements Alquiler{
    String referencia;
    int metros;
    int habitaciones;
    int banos;
    int zona;
    int plantas;

    Viviendas() {

    }

    Viviendas(String referencia, int metros, int habitaciones, int banos, int zona, int plantas) {
        this.referencia = referencia;
        this.metros = metros;
        this.habitaciones = habitaciones;
        this.banos = banos;
        this.zona = zona;
        this.plantas = plantas;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public int getMetros() {
        return metros;
    }

    public void setMetros(int metros) {
        this.metros = metros;
    }

    public int getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(int habitaciones) {
        this.habitaciones = habitaciones;
    }

    public int getBanos() {
        return banos;
    }

    public void setBanos(int banos) {
        this.banos = banos;
    }

    public int getZona() {
        return zona;
    }

    public void setZona(int zona) {
        this.zona = zona;
    }

    public int getPlantas() {
        return plantas;
    }

    public void setPlantas(int plantas) {
        this.plantas = plantas;
    }

    @Override
    public float calcularAlquiler(Viviendas vivienda) {
        return 0;
    }
}
