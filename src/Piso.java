public class Piso extends Viviendas implements Alquiler {
    int planta_piso;

    public int getPlanta_piso() {
        return planta_piso;
    }

    public void setPlanta_piso(int planta_piso) {
        this.planta_piso = planta_piso;
    }

    public float getPrecio_comunidad() {
        return precio_comunidad;
    }

    public void setPrecio_comunidad(float precio_comunidad) {
        this.precio_comunidad = precio_comunidad;
    }

    float precio_comunidad;

    Piso(String referencia, int metros, int habitaciones, int banos, int zona, int plantas, int planta_piso, float precio_comunidad){
        super(referencia, metros, habitaciones,banos,zona, plantas);
        this.planta_piso = planta_piso;
        this.precio_comunidad = precio_comunidad;
    }

    @Override
    public float calcularAlquiler(Viviendas vivienda) {

        switch (zona){
            case 1:
                return (7.5f * metros) + precio_comunidad;
            case 2:
                return (6.5f * metros) + precio_comunidad;
            case 3:
                return (5f * metros) + precio_comunidad;
            case 4:
                return (4.5f * metros) + precio_comunidad;
        }
        return 0f;
    }
}
