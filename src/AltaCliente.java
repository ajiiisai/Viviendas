
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.table.DefaultTableModel;

public class AltaCliente extends JFrame {

    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;


    /**
     * Create the frame.
     */
    public AltaCliente() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 413, 600);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblAltaCliente = new JLabel("PROPIETARIOS");
        lblAltaCliente.setFont(new Font("Tahoma", Font.BOLD, 26));
        lblAltaCliente.setBounds(96, 4, 204, 32);
        contentPane.add(lblAltaCliente);

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        panel.setBounds(6, 47, 385, 96);
        contentPane.add(panel);
        panel.setLayout(null);

        JLabel lblDni = new JLabel("DNI");
        lblDni.setBounds(6, 19, 24, 14);
        panel.add(lblDni);

        textField = new JTextField();
        textField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                if (Propietarios.compruebaDNI(textField.getText()) == true) {
                    textField.setBackground(Color.green);
                } else if (textField.getText().isEmpty()) {
                    textField.setBackground(Color.white);
                } else {
                    textField.setBackground(Color.red);
                }
            }
        });
        textField.setBounds(40, 16, 86, 20);
        panel.add(textField);
        textField.setColumns(10);

        JLabel lblNombre = new JLabel("Nombre");
        lblNombre.setBounds(170, 19, 46, 14);
        panel.add(lblNombre);

        textField_1 = new JTextField();
        textField_1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                textField_1.setBackground(Color.white);
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textField_1.getText().length() > 20) {
                    textField_1.setBackground(Color.red);
                    textField_1.setToolTipText("El nombre debe contener menos de 20 caracteres");
                } else if (textField_1.getText().isEmpty()) {
                    textField_1.setBackground(Color.white);
                } else {
                    textField_1.setBackground(Color.green);
                    textField_1.setToolTipText(null);
                }
            }
        });
        textField_1.setBounds(226, 16, 153, 20);
        panel.add(textField_1);
        textField_1.setColumns(10);

        JLabel lblDireccin = new JLabel("Direcci\u00F3n");
        lblDireccin.setBounds(6, 47, 62, 14);
        panel.add(lblDireccin);

        textField_2 = new JTextField();
        textField_2.setBounds(62, 44, 228, 20);
        panel.add(textField_2);
        textField_2.setColumns(10);

        JLabel lblEdad = new JLabel("Edad");
        lblEdad.setBounds(311, 47, 34, 14);
        panel.add(lblEdad);

        textField_3 = new JTextField();
        textField_3.setBounds(345, 44, 34, 20);
        panel.add(textField_3);
        textField_3.setColumns(10);

        JLabel lblTelfono = new JLabel("Tel\u00E9fono");
        lblTelfono.setBounds(6, 72, 62, 14);
        panel.add(lblTelfono);

        textField_4 = new JTextField();
        textField_4.setBounds(58, 69, 116, 20);
        panel.add(textField_4);
        textField_4.setColumns(10);

        JPanel panel_1 = new JPanel();
        panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "CalculoAlquiler que posee", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        panel_1.setBounds(0, 156, 403, 174);
        contentPane.add(panel_1);
        panel_1.setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 22, 383, 107);
        panel_1.add(scrollPane);

        String col[] = {"Tipo", "Referencia", "Metros", "Habitaciones", "Baños", "Zona"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        JTable table = new JTable(tableModel) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        scrollPane.setViewportView(table);

        JButton btnEliminarSeleccionados = new JButton("Eliminar Seleccionados");
        btnEliminarSeleccionados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (table.getSelectedRow() != -1) {
                    for (int i = 0; i < Main.propietarios.size(); i++) {
                        if (Main.propietarios.get(i).nombre.equals(textField_1.getText())) {
                            for (int j = 0; j < 3; j++) {
                                if (Main.propietarios.get(i).viviendas[j] != null) {
                                    if (Main.propietarios.get(i).viviendas[j].getReferencia() == tableModel.getValueAt(table.getSelectedRow(), 1)) {
                                        Main.propietarios.get(i).viviendas[j] = null;
                                    }
                                }
                            }
                        }
                    }
                    tableModel.removeRow(table.getSelectedRow());
                }
            }
        });
        btnEliminarSeleccionados.setBounds(254, 140, 139, 23);
        panel_1.add(btnEliminarSeleccionados);

        JPanel panel_2 = new JPanel();
        panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Resto de pisos", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        panel_2.setBounds(6, 341, 395, 174);
        contentPane.add(panel_2);
        panel_2.setLayout(null);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(6, 16, 383, 107);
        panel_2.add(scrollPane_1);

        DefaultTableModel tableModel1 = new DefaultTableModel(col, 0);
        JTable table_1 = new JTable(tableModel1) {
            public boolean isCellEditable(int row, int column) {
                return false;//This causes all cells to be not editable
            }
        };
        for (Viviendas p : Main.viviendas) {
            Object[] data = {p.getClass().getSimpleName(), p.getReferencia(), p.getMetros(), p.getHabitaciones(), p.getBanos(), p.getZona()};
            tableModel1.addRow(data);
        }
        scrollPane_1.setViewportView(table_1);

        JButton btnAadirSeleccionados = new JButton("A\u00F1adir Seleccionados");
        btnAadirSeleccionados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = table_1.getSelectedRow();
                Object[] data = {tableModel1.getValueAt(row, 0), tableModel1.getValueAt(row, 1), tableModel1.getValueAt(row, 2),
                        tableModel1.getValueAt(row, 3), tableModel1.getValueAt(row, 4), tableModel1.getValueAt(row, 5)};
                if (tableModel.getRowCount() < 3) {
                    tableModel.addRow(data);
                    tableModel1.removeRow(row);
                } else {
                    JOptionPane.showMessageDialog(contentPane,
                            "Solo 3 pisos por propietario",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                for (int i = 0; i < Main.propietarios.size(); i++) {
                    if (Main.propietarios.get(i).nombre.equals(textField_1.getText())) {
                        for (int j = 0; j < 3; j++) {
                            if (Main.propietarios.get(i).viviendas[j] == null) {
                                for (Viviendas p : Main.viviendas) {
                                    if (p.getReferencia() == data[1]) {
                                        Main.propietarios.get(i).viviendas[j] = p;
                                        j = 3;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        btnAadirSeleccionados.setBounds(244, 134, 145, 23);
        panel_2.add(btnAadirSeleccionados);

        JButton btnDarDeAlta = new JButton("DAR DE ALTA");
        btnDarDeAlta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boolean existe = false;
                    if (tableModel.getRowCount() < 1) {
                        if (Propietarios.compruebaDNI(textField.getText())) {
                            for (int i = 0; i < Main.propietarios.size(); i++) {
                                if (Main.propietarios.get(i).DNI.equals(textField.getText())) {
                                    existe = true;
                                    textField_1.setText(Main.propietarios.get(i).nombre);
                                    textField_2.setText(Main.propietarios.get(i).direccion);
                                    textField_3.setText(String.valueOf(Main.propietarios.get(i).edad));
                                    textField_4.setText(Main.propietarios.get(i).telefono);
                                    for (int j = 0; j < 3; j++) {
                                        if (Main.propietarios.get(i).viviendas[j] != null) {
                                            Object[] data = {Main.propietarios.get(i).viviendas[j].getClass().getSimpleName(), Main.propietarios.get(i).viviendas[j].getReferencia(),
                                                    Main.propietarios.get(i).viviendas[j].getMetros(), Main.propietarios.get(i).viviendas[j].getHabitaciones(),
                                                    Main.propietarios.get(i).viviendas[j].getBanos(), Main.propietarios.get(i).viviendas[j].getZona()};
                                            tableModel.addRow(data);
                                            String pisosNoMostrar = Main.propietarios.get(i).viviendas[j].getReferencia();
                                            for (int z = 0; z < tableModel1.getRowCount(); z++) {
                                                if (tableModel1.getValueAt(z, 1) == pisosNoMostrar) {
                                                    tableModel1.removeRow(z);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (!existe) {
                                Propietarios propietario = new Propietarios(textField_1.getText(), textField.getText(), Integer.valueOf(textField_3.getText()),
                                        textField_2.getText(), textField_4.getText(), new Viviendas[]{null, null, null});
                                Main.propietarios.add(propietario);
                                JOptionPane.showMessageDialog(getContentPane(), "Propietario: " + textField_1.getText() + " ha sido registrado con exito", "ALTA", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(getContentPane(), "Hay casillas sin rellenar, deben estar todas rellenadas", "ALTA", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }//
                } catch (java.lang.NumberFormatException errr) {
                    JOptionPane.showMessageDialog(getContentPane(), "La edad debe ser un numero", "ALTA", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnDarDeAlta.setBounds(145, 526, 111, 23);
        contentPane.add(btnDarDeAlta);
    }
}
