public class Chalet extends Unifamiliar implements Alquiler {
    public boolean piscina;

    Chalet(String referencia, int metros, int habitaciones, int banos, int zona, int plantas, float metros_jardin, boolean piscina){
        super(referencia, metros, habitaciones, banos, zona, plantas, metros_jardin);
        this.piscina = piscina;
    }

    public boolean getPiscina() {
        return piscina;
    }

    public void setPiscina(boolean piscina) {
        this.piscina = piscina;
    }

    @Override
    public float calcularAlquiler(Viviendas vivienda) {
        switch (zona){
            case 1:
                if(piscina){
                    return (7.5f * metros) + 120 + 100;
                }else {
                    return (7.5f * metros) + 120;
                }
            case 2:
                if(piscina){
                    return (6.5f * metros) + 120 + 100;
                }else {
                    return (6.5f * metros) + 120;
                }
            case 3:
                if(piscina){
                    return (5f * metros) + 120 + 100;
                }else {
                    return (5f * metros) + 120;
                }
            case 4:
                if(piscina){
                    return (4.5f * metros) + 120 + 100;
                }else {
                    return (4.5f * metros) + 120;
                }
        }
        return 0f;
    }
}
